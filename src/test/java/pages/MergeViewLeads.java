package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class MergeViewLeads extends ProjectMethods{
	
	public void clickfindleads(String errorMag) throws InterruptedException
	{
		WebElement elegoback = locateElement("linktext", "Find Leads");
		click(elegoback);
		MergeFindLeadsPage1 mod = new MergeFindLeadsPage1();
		type(locateElement("xpath", "//input[@name='id']"),mod.leadId);
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		Thread.sleep(1000);
		WebElement eleVerify = locateElement("class", "x-paging-info");
		verifyExactText(eleVerify, errorMag);
	
	}

}
