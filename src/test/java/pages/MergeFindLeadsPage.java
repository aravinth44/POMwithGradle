package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class MergeFindLeadsPage extends ProjectMethods {
	
	public MergeFindLeadsPage typeFirstname(String data)
	{
		WebElement eleFstName = locateElement("name", "firstName");
		type(eleFstName, data);
		return this;
	}
	
	
	
	public MergeFindLeadsPage clickFindleads() throws InterruptedException
	{
		WebElement eleClick = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleClick);
		Thread.sleep(1000);
		//String leadId = getText(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
		return this;
	}
	

	
	public MergeLeadsPage clickFirrstresult()
	{
		WebElement eleFrstRest = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(eleFrstRest);
		switchToWindow(0);
		return new MergeLeadsPage();
		
	}
	
	

}
