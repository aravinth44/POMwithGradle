package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods {
	
	public EditLeadPage clickEdit()
	{
		WebElement eleClickedit = locateElement("linktext", "Edit");
		click(eleClickedit);
		return new EditLeadPage();
	}
	
	public void readCompanyname()
	{
		WebElement eleCompName = locateElement("id", "viewLead_companyName_sp");
		String newCompName = eleCompName.getText();
		System.out.println(newCompName);
		
	}

}
