package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class MergeLeadsPage extends ProjectMethods{
	
	public MergeFindLeadsPage1 clickFromlead()
	{
		WebElement eleFromlead = locateElement("xpath", "(//img[@alt='Lookup'])[1]");
		click(eleFromlead);
		switchToWindow(1);
		return new MergeFindLeadsPage1();
		
	}
	
	public MergeFindLeadsPage clickTolead()
	{
		WebElement eleTolead = locateElement("xpath", "(//img[@alt='Lookup'])[2]");
		click(eleTolead);
		switchToWindow(1);
		return new MergeFindLeadsPage();
		
	}
	
	public MergeViewLeads clickMergelead()
	{
		WebElement eleClickMerge = locateElement("linktext", "Merge");
		click(eleClickMerge);
		acceptAlert();
		return new MergeViewLeads();
		
	}

}
