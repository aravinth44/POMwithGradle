package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods {

	public FindLeadsPage clickPhonetab()
	{
		WebElement elePhonetab = locateElement("xpath", "//span[text()='Phone']");
		click(elePhonetab);
		return this;
	}
	public FindLeadsPage typePhoneNum(String data)
	{
		WebElement elePhoneNum = locateElement("xpath", "//input[@name='phoneNumber']");
		type(elePhoneNum, data);
		return this;
	}
	
	public FindLeadsPage clickFindleads() throws InterruptedException
	{
		WebElement eleFindleads = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindleads);
		Thread.sleep(3000);
		return this;
	}
	
	public ViewLeadPage clickLeadId()
	{
		WebElement eleLeadID = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(eleLeadID);
		return new ViewLeadPage();
		
	}
	

}
