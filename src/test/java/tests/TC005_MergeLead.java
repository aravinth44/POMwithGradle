package tests;

import org.testng.annotations.BeforeClass;

//import org.junit.Test;

import org.testng.annotations.Test;

import pages.MyHomePage;
import wdMethods.ProjectMethods;

public class TC005_MergeLead extends ProjectMethods {
	
	@BeforeClass
	public void setData() {
		testCaseName = "TC005_MergeLead";
		testCaseDescription ="Merge two leads";
		category = "Smoke";
		author= "Babu";
		dataSheetName="TC005";
	}
	@Test(dataProvider="fetchData")
	public  void mergeLead(String fromLead, String toLead,String errorMag) throws InterruptedException   {
		
		new MyHomePage().clickLeads()
		.clickMergeLead().clickFromlead().typeFirstname(fromLead).clickFindleads()
		.clickFirrstresult().clickTolead().typeFirstname(toLead).clickFindleads()
		.clickFirrstresult().clickMergelead().clickfindleads(errorMag);
						
	}

}
